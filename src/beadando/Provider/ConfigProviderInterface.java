package beadando.Provider;

/**
 * Describes how a ConfigProvider object should look like.
 */
interface ConfigProviderInterface {

    ParsedConfigObject parsedConfigObject = null;

    default ParsedConfigObject getParsedConfig() {
        return parsedConfigObject;
    }

}
