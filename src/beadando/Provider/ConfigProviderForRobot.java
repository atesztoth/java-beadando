package beadando.Provider;

import beadando.Robot.Phase;

import java.util.HashMap;

@FunctionalInterface
public interface ConfigProviderForRobot {
    HashMap<Phase, HashMap<String, Integer>> getMeAConfig();
}
