package beadando.Provider;

import beadando.Product.Product1;
import beadando.Product.Product2;
import beadando.Product.Product3;
import beadando.Robot.Phase;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Random;

public class RobotConfigProvider {

    private final int maxProducts;
    private final int minProducts;

    public RobotConfigProvider(int maxProducts, int minProducts) {
        this.maxProducts = maxProducts;
        this.minProducts = minProducts;
    }

    public HashMap<Phase, HashMap<String, Integer>> getAConfig() {
        HashMap<Phase, HashMap<String, Integer>> phaseProducts = new HashMap<>();
        // I know order breaks, but this looks to good to change :(
        Arrays.stream(Phase.values()).forEach(phase -> phaseProducts.put(phase, productsForPhase()));
        return phaseProducts;
    }

    private HashMap<String, Integer> productsForPhase() {
        HashMap<String, Integer> prodNumbers = new HashMap<>();
        Random rand = new Random();

        prodNumbers.put(Product1.idString(), rand.nextInt(maxProducts) + minProducts);
        prodNumbers.put(Product2.idString(), rand.nextInt(maxProducts) + minProducts);
        prodNumbers.put(Product3.idString(), rand.nextInt(maxProducts) + minProducts);

        return prodNumbers;
    }

}
