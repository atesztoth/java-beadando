package beadando.Provider;

/**
 * Indicates that parsing functionally can be decoupled from ConfigProvider,
 * only in this case it does not worth it.
 */
public interface ConfigParser {

    void parseConfigFile(String fileName) throws Exception;

}
