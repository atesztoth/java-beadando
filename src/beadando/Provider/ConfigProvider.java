package beadando.Provider;

import javafx.util.Pair;

import java.io.*;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import beadando.Exception.UnkownConfigVarException;
import beadando.Exception.WronglyFormattedConfigException;

import static beadando.Provider.ConfigVars.*;

public class ConfigProvider implements ConfigProviderInterface, ConfigParser {

    public static void main(String[] args) throws Exception {
        ConfigProvider configProvider = ConfigProvider.getConfigProviderInstance();
        ParsedConfigObject parsedConfigObject = configProvider.getParsedConfig();

        System.out.println(parsedConfigObject.getNumberOfRobots());
        int[][] initialNumberOfProds = parsedConfigObject.getInitialNumberOfProducts();
        int[][] phaseStepNum = parsedConfigObject.getPhaseStepNumbers();

        for (int i = 0; i < initialNumberOfProds.length; i++) {
            for (int j = 0; j < initialNumberOfProds[i].length; j++) {
                System.out.println("initialNumberOfProds[" + i + "][" + j + "]: " + initialNumberOfProds[i][j]);
            }
        }

        for (int i = 0; i < phaseStepNum.length; i++) {
            for (int j = 0; j < phaseStepNum[i].length; j++) {
                System.out.println("phaseStepNum[" + i + "][" + j + "]: " + phaseStepNum[i][j]);
            }
        }

    }

    private static ConfigProvider configProviderInstance = null;
    private InputStream inputStream;
    private InputStreamReader inputStreamReader;
    private BufferedReader bufferedReader;
    private ParsedConfigObject parsedConfigObject = null;
    private int numberOfRobots = 0;

    public static ConfigProvider getConfigProviderInstance() throws Exception {
        if (null == configProviderInstance) return new ConfigProvider();
        return configProviderInstance;
    }

    private ConfigProvider() throws Exception {
        this.parseConfigFile("config.txt");
    }

    @Override
    public ParsedConfigObject getParsedConfig() {
        return parsedConfigObject;
    }

    // ConfigParser
    @Override
    public void parseConfigFile(String fileName) throws Exception {
        openFile(fileName);
        // RobotNo => Config(Integer[]) values
        HashMap<Integer, ArrayList<Integer>> initProdNums = new HashMap<>();
        HashMap<Integer, ArrayList<Integer>> phaseStepNums = new HashMap<>();

        String line;
        while ((line = bufferedReader.readLine()) != null) {
            if ("".equals(line) || '#' == line.charAt(0)) continue; // comment
            String[] splittedLine = line.split(":");
            Pair<String, String> configVar = new Pair<>(splittedLine[0], splittedLine[1].trim());

            if (NUM_OF_ROBOTS.stringValue.equals(configVar.getKey())) {
                numberOfRobots = Integer.valueOf(configVar.getValue());
            } else {
                String[] second = configVar.getKey().split("_");
                Pair<String, String> keyParts = new Pair<>(second[0], second[1]);

                switch (keyParts.getKey()) {
                    case "robot":
                        int robotNo = Integer.valueOf(keyParts.getValue());
                        if (null != initProdNums.get(robotNo))
                            throw new WronglyFormattedConfigException("Repeating robot initial data! (" + robotNo + " robot)");

                        initProdNums.put(robotNo, createIntArrayFromStringArray(configVar.getValue().split(" ")));
                        break;
                    case "phase":
                        int phaseNo = Integer.valueOf(keyParts.getValue());
                        if (null != phaseStepNums.get(phaseNo))
                            throw new WronglyFormattedConfigException("Repeating phase step data! (" + phaseNo + " phase)");

                        phaseStepNums.put(phaseNo, createIntArrayFromStringArray(configVar.getValue().split(" ")));
                        break;
                    default:
                        throw new UnkownConfigVarException(keyParts.getKey());
                }
            }
        }

        // final checks:
        if (numberOfRobots != initProdNums.size())
            throw new WronglyFormattedConfigException("Num of robots does not match initialized robots number.");
        if (ConfigVars.PHASE_NUMBER.intValue != phaseStepNums.size())
            throw new WronglyFormattedConfigException("Too many phases have been declared. Max: " + ConfigVars.PHASE_NUMBER.stringValue);

        final int finalNumberOfRobots = numberOfRobots;
        final int[][] initialNumberOfProducts = transformHashMap(initProdNums, "robot");
        final int[][] phaseStepNumbers = transformHashMap(phaseStepNums, "phase");

        this.parsedConfigObject = new ParsedConfigObject() {
            @Override
            public int getNumberOfRobots() {
                return finalNumberOfRobots;
            }

            @Override
            public int[][] getInitialNumberOfProducts() {
                return initialNumberOfProducts;
            }

            @Override
            public int[][] getPhaseStepNumbers() {
                return phaseStepNumbers;
            }
        };

        closeAll();
    }

    // Todo: if time remains, create a map for Array or smthng like that
    private ArrayList<Integer> createIntArrayFromStringArray(String[] array) {
        ArrayList<Integer> result = new ArrayList<>();
        for (String anArray : array) result.add(Integer.valueOf(anArray));

        return result;
    }

    // Lets use Stream API!
    private int[][] transformHashMap(HashMap<Integer, ArrayList<Integer>> hashMap) throws WronglyFormattedConfigException {
        return transformHashMap(hashMap, "");
    }

    private int[][] transformHashMap(HashMap<Integer, ArrayList<Integer>> hashMap, String type) throws WronglyFormattedConfigException {
        int[][] result = new int[hashMap.size()][hashMap.get(hashMap.keySet().iterator().next()).size()];

        for (Map.Entry<Integer, ArrayList<Integer>> entry : hashMap.entrySet()) {
            int key = entry.getKey();
            int[] value = entry.getValue().stream().mapToInt(i -> i).toArray();

            if (type.equals("robot") && (key > numberOfRobots || key < 1))
                throw new WronglyFormattedConfigException("Unexpected robot index: " + key);


            if (type.equals("phase") && (key > PHASE_NUMBER.intValue || key < 1)) {
                throw new WronglyFormattedConfigException("Unexpected phase index: " + key);
            }

            result[key - 1] = value;
        }

        return result;
    }

    // Private
    private void openFile(String fileName) throws FileNotFoundException {
        inputStream = new FileInputStream(fileName);
        inputStreamReader = new InputStreamReader(inputStream, Charset.forName("UTF-8"));
        bufferedReader = new BufferedReader(inputStreamReader);
    }

    private void closeAll() throws IOException {
        inputStream.close();
        inputStreamReader.close();
        bufferedReader.close();
    }
}
