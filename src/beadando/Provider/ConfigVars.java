package beadando.Provider;

enum ConfigVars {
    NUM_OF_ROBOTS("num_of_robots"),
    REPR_ROBOT("robot"),
    REPR_PHASE("phase"),
    PHASE_NUMBER(3);

    public final String stringValue;
    public final int intValue;

    ConfigVars(String stringValue) {
        this.stringValue = stringValue;
        int casted;
        try {
            casted = Integer.valueOf(stringValue);
        } catch (NumberFormatException e) {
            casted = 0;
        }
        this.intValue = casted;
    }

    ConfigVars(Integer intValue) {
        this.stringValue = String.valueOf(intValue);
        this.intValue = intValue;
    }
}
