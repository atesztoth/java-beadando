package beadando.Provider;

/**
 * An interface describing what a result of a config parser should look like. Easily usable through
 * the whole application.
 */
public interface ParsedConfigObject {

    // Returns the number of robots.
    int getNumberOfRobots();

    // Describes how many products a robots should have initially.
    int[][] getInitialNumberOfProducts();

    // Represents how many products a robot must have to step a phase.
    int[][] getPhaseStepNumbers();

}
