package beadando.Exception;

public class WronglyFormattedConfigException extends Exception {
    public WronglyFormattedConfigException() {
        super();
    }

    public WronglyFormattedConfigException(String message) {
        super(message);
    }
}
