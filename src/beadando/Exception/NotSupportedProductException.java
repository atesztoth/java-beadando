package beadando.Exception;

public class NotSupportedProductException extends Exception {

    public NotSupportedProductException() {
    }

    public NotSupportedProductException(String message) {
        super(message);
    }
}
