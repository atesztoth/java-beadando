package beadando.Exception;

public class UnkownConfigVarException extends  Exception {
    public UnkownConfigVarException() {
        super();
    }

    public UnkownConfigVarException(String message) {
        super(message);
    }
}
