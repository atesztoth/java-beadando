package beadando.Exception;

public class NoWorkerProvidedException extends Exception {
    public NoWorkerProvidedException() {
    }

    public NoWorkerProvidedException(String message) {
        super(message);
    }
}
