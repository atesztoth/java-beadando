package beadando.Exception;

public class WrongPhaseIntervalException extends Exception {
    public WrongPhaseIntervalException() {
        super();
    }

    public WrongPhaseIntervalException(String message) {
        super(message);
    }
}
