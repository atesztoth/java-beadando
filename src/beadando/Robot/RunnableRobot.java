package beadando.Robot;

public class RunnableRobot implements Runnable {
    private Robot robot;

    public RunnableRobot(Robot r) {
        robot = r;
    }

    @Override
    public void run() {
        robot.start();
    }
}
