package beadando.Robot;

import beadando.Exception.NoWorkerProvidedException;
import beadando.Exception.NotSupportedProductException;
import beadando.Product.*;
import beadando.Provider.ConfigProviderForRobot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.concurrent.atomic.AtomicBoolean;

import static java.lang.System.exit;
import static java.lang.Thread.sleep;

public class Robot {

    private final String name;
    private final int serialNumber;
    private Phase phase = Phase.ONE;
    private AtomicBoolean isDone = new AtomicBoolean(false);
    private volatile Product lastCreatedProduct = null;
    private Worker workerLambda = null;
    private Random rand = new Random();

    // This will store info about inventory of actual robot instance.
    private volatile HashMap<String, ArrayList<ProductInterface>> inventory = new HashMap<>();

    // This stores info about how many products the robot must have in order to step phase.
    private HashMap<Phase, HashMap<String, Integer>> phaseProductNumber = new HashMap<>();

    public Robot(String name, int serialNumber) {
        this.name = name;
        this.serialNumber = serialNumber;
    }

    public String getName() {
        return name;
    }

    void start() {
        System.out.printf("Robot %s is running! %s", name, System.lineSeparator());

        while (!isDone.get()) {
            int simulateWorkTill = rand.nextInt(this.phase.t2) + this.phase.t1;
            try {
                sleep(simulateWorkTill);

                if (!canProcess()) {
                    System.out.println(name + " reports that it cannot process.");
                    goodNight();
                    continue;
                }

                try {
                    System.out.println(name + " reports will process.");
                    processProducts();
                    if (isDone.get()) {
                        System.out.printf("%s robot is done. %s", name, System.lineSeparator());
                        break;
                    }
                } catch (NoWorkerProvidedException e) {
                    System.err.printf("%s could not process a product.", name);
                    e.getMessage();
                    e.printStackTrace();
                }
            } catch (InterruptedException e) {
                System.err.printf("%s robot could not sleep! %s", name, System.lineSeparator());
                e.printStackTrace();
                exit(1);
            }
        }
    }

    public void setPhase(Phase phase) {
        this.phase = phase;
    }

    public void provideMeAConfig(ConfigProviderForRobot configProviderForRobot) {
        phaseProductNumber = configProviderForRobot.getMeAConfig();
    }

    public void reveiceProduct(ProductInterface product) throws NotSupportedProductException {
        ArrayList<String> supportedProducts = getSupportedProducts();
        if (!supportedProducts.contains(product.getIdString()))
            throw new NotSupportedProductException("Maybe you have provided a base Product class.");

        ArrayList<ProductInterface> prodsOfType = inventory.get(product.getIdString());
        if (null == prodsOfType) prodsOfType = new ArrayList<>();
        prodsOfType.add(product);
        inventory.put(product.getIdString(), prodsOfType);
    }

    // MARK: - Private part

    /**
     * This method is responsible for deciding if a worker lambda can be applied or not.
     */
    public synchronized boolean canProcess() {
        HashMap<String, Integer> requirements = phaseProductNumber.get(phase);
        if (null == requirements) return false; // Probably has ran too soon.

        // stop processing if not possible
        return requirements.keySet().stream().allMatch(key -> (null != inventory.get(key) ? inventory.get(key).size() : 0) >= requirements.get(key));
    }

    /**
     * Processes product using the lambda supplied by the controller.
     *
     * @throws NoWorkerProvidedException When no worker lambda is provided.
     */
    private synchronized void processProducts() throws NoWorkerProvidedException {
        HashMap<String, Integer> requirements = phaseProductNumber.get(phase);
        if (null == workerLambda) throw new NoWorkerProvidedException("No lambda has been provided for " + name);

        HashMap<String, ArrayList<ProductInterface>> itemsToProcess = new HashMap<>();
        requirements.keySet().forEach(productKey -> {
            ArrayList<ProductInterface> inventoryItems = inventory.get(productKey);
            ArrayList<ProductInterface> productsForLambda = new ArrayList<>();

            // According to the description of the task, items should be picked randomly.
            for (int i = 0; i < requirements.get(productKey); i++) {
                int nextElementIndex = rand.nextInt(inventoryItems.size());
                productsForLambda.add(inventoryItems.remove(nextElementIndex));
            }

            itemsToProcess.put(productKey, productsForLambda);
        });

        lastCreatedProduct = workerLambda.apply(itemsToProcess);
        if (phase == Phase.THREE) isDone.set(true);
    }

    /**
     * A list a product ids a robot can accept.
     *
     * @return ArrayList
     */
    private ArrayList<String> getSupportedProducts() {
        ArrayList<String> supportedProducts = new ArrayList<>();
        supportedProducts.add(Product1.idString());
        supportedProducts.add(Product2.idString());
        supportedProducts.add(Product3.idString());

        return supportedProducts;
    }

    // MARK: - synchronized
    public synchronized boolean isDone() {
        return isDone.get();
    }

    public synchronized void setWorkerLambda(Worker workerLambda) {
        this.workerLambda = workerLambda;
    }

    public synchronized Product getLastCreatedProduct() {
        return lastCreatedProduct;
    }

    public synchronized void removeLastCreatedProduct() {
        lastCreatedProduct = null;
    }

    public synchronized Phase getPhase() {
        return phase;
    }

    public synchronized void wake() {
        notify();
    }

    private synchronized void goodNight() throws InterruptedException {
        wait();
    }
}
