package beadando.Robot;

public enum Phase {

    ONE(1000, 3400),
    TWO(3000, 6300),
    THREE(6100, 10000);

    // Minimum of working time interval
    public final int t1;
    // Maximum of working time interval
    public final int t2;

    Phase(int t1, int t2) throws ExceptionInInitializerError {
        if (t2 <= t1) throw new ExceptionInInitializerError("At a phase, t2 must be greater than t1!");

        this.t1 = t1;
        this.t2 = t2;
    }
}