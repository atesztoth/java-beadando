package beadando.Robot;

import beadando.Product.Product;
import beadando.Product.ProductInterface;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.function.Function;

public interface Worker extends Function<HashMap<String, ArrayList<ProductInterface>>, Product> {
}
