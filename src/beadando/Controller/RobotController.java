package beadando.Controller;

import beadando.Exception.NotSupportedProductException;
import beadando.Product.*;
import beadando.Provider.ConfigProvider;
import beadando.Provider.ParsedConfigObject;
import beadando.Provider.RobotConfigProvider;
import beadando.Robot.Phase;
import beadando.Robot.Robot;
import beadando.Robot.RunnableRobot;
import beadando.Robot.Worker;
import javafx.util.Pair;

import java.util.ArrayList;
import java.util.Random;
import java.util.function.Function;
import java.util.function.Supplier;

import static java.lang.System.exit;
import static java.lang.Thread.sleep;

public class RobotController {

    // Class constants
    private static final int t3 = 2000;
    private static final int t4 = 4000;

    // Random generator
    private Random rand = new Random();

    // For storing thread references related to robots.
    private ArrayList<Thread> robotsInDisguise = new ArrayList<>();
    // For storing robot references in order to access 'em.
    private ArrayList<Robot> robots = new ArrayList<>();
    private ParsedConfigObject config;

    // Worker lambdas:
    private Supplier<Product1> product1Supplier = () -> new Product1("NewProduct", Color.pickARandomOne());
    private Supplier<Product2> product2Supplier = () -> new Product2("Prod2", String.valueOf(rand.nextInt(99999) + 10000));
    private Supplier<Product3> product3Supplier = () -> new Product3("Test", rand.nextInt(200) + 1);

    // Dummy worker lambdas
    private Function<Pair<Robot, Worker>, Worker> workerLambdaCreator = (robotAndWorker) -> (Worker) hashMap -> {
        System.out.printf("%s is processing its products. %s", robotAndWorker.getKey().getName(), System.lineSeparator());
        return robotAndWorker.getValue().apply(hashMap);
    };

    public static void main(String[] args) throws InterruptedException {
        RobotController controller = new RobotController();
        controller.startRobots();
    }

    private RobotController() {
        try {
            ConfigProvider configProvider = ConfigProvider.getConfigProviderInstance();
            config = configProvider.getParsedConfig();
        } catch (Exception e) {
            System.err.println("Unable to initialize configprovider. Exiting.");
            e.printStackTrace();
            exit(1);
        }
    }

    private void startRobots() throws InterruptedException {
        for (int i = 0; i < config.getNumberOfRobots(); i++) {
            final int robotNum = i;
            final int serialNum = rand.nextInt(9999) + 1000;
            Robot r = new Robot("Robotusin " + robotNum, serialNum);
            r.provideMeAConfig(() -> (new RobotConfigProvider(7, 1).getAConfig()));

            // Supply start package for robots:
            int[] productNumbers = config.getInitialNumberOfProducts()[robotNum];
            for (int j = 0; j < productNumbers.length; j++) {
                try {
                    switch (j) {
                        case 0:
                            for (int k = 0; k < productNumbers[j]; k++)
                                r.reveiceProduct(product1Supplier.get());
                            break;
                        case 1:
                            for (int k = 0; k < productNumbers[j]; k++)
                                r.reveiceProduct(product2Supplier.get());
                            break;
                        default:
                            for (int k = 0; k < productNumbers[j]; k++)
                                r.reveiceProduct(product3Supplier.get());
                    }
                } catch (NotSupportedProductException e) {
                    System.err.printf("Wrong product type provided to: %s %s", r.getName(), System.lineSeparator());
                }
            }

            r.setPhase(Phase.ONE);
            r.setWorkerLambda(idsAndProducts -> {
                System.out.printf("%s is processing its products. %s", r.getName(), System.lineSeparator());
                return new Product(r.getName() + " - by worker.");
            });

            robots.add(r);
            final Thread robotThread = new Thread(new RunnableRobot(r));
            robotsInDisguise.add(robotThread);
            robotThread.start();
        }

        robotControllingLogic();

        robotsInDisguise.forEach(thread -> {
            try {
                thread.join();
            } catch (InterruptedException e) {
                System.err.println("A tread failed at join!");
                e.printStackTrace();
            }
        });

        System.out.println("Controller is done! Bye bye!");
    }

    private void robotControllingLogic() throws InterruptedException {
        do {
            int sleepTime = rand.nextInt(t4) + t3;
            System.out.printf("Controller is going to sleep for %s. \n", sleepTime);
            sleep(sleepTime);
            System.out.println("Controller is awake. \n");

            robots.forEach(robot -> {
                if (robot.isDone()) return;

                Product lastCreatedProduct = robot.getLastCreatedProduct();
                if (null == lastCreatedProduct && robot.canProcess()) return;

                if (null != lastCreatedProduct) {
                    manageRobotPhaseStep(robot);
                    return;
                }

                giveProductsToRobot(robot);
                robot.wake();
            });

        } while (!robots.stream().allMatch(Robot::isDone));
    }

    private void giveProductsToRobot(Robot robot) {
        Phase[] phases = Phase.values();
        for (int i = 0; i < phases.length; i++) {
            if (robot.getPhase() != phases[i]) continue;

            try {
                for (int j = 0; j < i + 1; j++) robot.reveiceProduct(product1Supplier.get());
                for (int j = 0; j < i + 1; j++) robot.reveiceProduct(product2Supplier.get());
                for (int j = 0; j < i + 1; j++) robot.reveiceProduct(product3Supplier.get());
            } catch (NotSupportedProductException e) {
                System.err.println(robot.getName() + " could not recieve a product.");
                e.printStackTrace();
            }

            break;
        }
    }

    private void manageRobotPhaseStep(Robot robot) {
        robot.removeLastCreatedProduct();

        switch (robot.getPhase()) {
            case ONE:
                robot.setPhase(Phase.TWO);
                robot.setWorkerLambda(workerLambdaCreator.apply(new Pair<>(
                        robot, hashMap -> new Product("RandomPhase2Name" + rand.nextInt(1000)))
                ));
            case TWO:
                robot.setPhase(Phase.THREE);
                robot.setWorkerLambda(workerLambdaCreator.apply(new Pair<>(
                        robot, hashMap -> new Product("RandomPhase3Name" + rand.nextInt(1000)))
                ));
                break;
            case THREE:
                System.err.println("Should never get here!");
                exit(1);
                break;
        }
    }

}
