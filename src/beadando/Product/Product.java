package beadando.Product;

public class Product implements ProductInterface{

    private String name;

    public Product(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static String idString() {
        return Product.class.getSimpleName();
    }

    public String getIdString() {
        return Product.idString();
    }
}
