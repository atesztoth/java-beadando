package beadando.Product;

public class Product2 extends Product {

    private final String serialNumber;

    public Product2(String name, String serialNumber) {
        super(name);
        this.serialNumber = serialNumber;
    }

    public static String idString() {
        return Product2.class.getSimpleName();
    }

    public String getIdString() {
        return Product2.idString();
    }
}
