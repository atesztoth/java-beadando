package beadando.Product;

import java.util.List;
import java.util.Random;

public enum Color {
    WHITE, BLUE, RED, GREEN, CYAN, MAGENTA;

    private static final List<Color> values = List.of(values());
    private static final int size = values.size();
    private static final Random rand = new Random();

    public static Color pickARandomOne() {
        return values.get(rand.nextInt(size));
    }
}
