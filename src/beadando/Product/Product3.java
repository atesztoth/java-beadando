package beadando.Product;

public class Product3 extends Product {

    private final int topSpeed;

    public Product3(String name, int topSpeed) {
        super(name);
        this.topSpeed = topSpeed;
    }

    public static String idString() {
        return Product3.class.getSimpleName();
    }

    public String getIdString() {
        return Product3.idString();
    }
}
