package beadando.Product;

public class Product1 extends Product {

    private final Color color;

    public Product1(String name, Color color) {
        super(name);
        this.color = color;
    }

    public static String idString() {
        return Product1.class.getSimpleName();
    }

    public String getIdString() {
        return Product1.idString();
    }
}
