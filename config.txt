## Sample config. Only line comments are supported btw.
num_of_robots: 2

## Initial number of products
## You can even switch variable order, or supply them fully un-ordered.
robot_1: 1 1 1
robot_2: 1 1 1

## Numbers for phase-stepping:
phase_1: 3 3 3
phase_2: 4 4 4
phase_3: 5 5 5